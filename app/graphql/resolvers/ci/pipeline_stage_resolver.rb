# frozen_string_literal: true

module Resolvers
  module Ci
    class PipelineStageResolver < BaseResolver
      alias_method :pipeline, :object

      type Types::Ci::StageType, null: true

      argument :id, GraphQL::Types::ID,
               required: false,
               description: 'ID of the stage.'

      argument :name, GraphQL::Types::String,
               required: false,
               description: 'Name of the stage.'

      def ready?(**args)
        raise ::Gitlab::Graphql::Errors::ArgumentError, 'One of id or name is required' unless args.size >= 1

        super
      end

      def resolve(id: nil, name: nil)
        stages = pipeline.stages

        if id.present?
          stages = stages.id_in(id)
        end

        if name.present?
          stages = stages.by_name(name)
        end

        stages.take
      end
    end
  end
end
