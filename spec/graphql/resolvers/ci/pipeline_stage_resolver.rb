# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Resolvers::Ci::PipelineStageResolver do
  include GraphqlHelpers

  let_it_be(:project) { create(:project, :repository, :public) }
  let_it_be(:pipeline) { create(:ci_pipeline, project: project) }
  let_it_be(:ci_stage_1) { create(:ci_stage_entity, name: 'Stage 1', pipeline: pipeline) }
  let_it_be(:ci_stage_2) { create(:ci_stage_entity, name: 'Stage 2', pipeline: pipeline) }

  describe '#resolve' do
    context 'when no parameters are present' do
      it "raises an argument error" do
        expect_graphql_error_to_be_created(Gitlab::Graphql::Errors::ArgumentError, 'One of id or name is required') do
          resolve(described_class, obj: pipeline)
        end
      end
    end

    context 'when id is present' do
      it "returns the stage of the id" do
        stage = resolve(described_class, obj: pipeline, args: { id: ci_stage_1.id })

        expect(stage).to eq(ci_stage_1)
      end
    end

    context 'when name is present' do
      it "returns the stage of the name" do
        stage = resolve(described_class, obj: pipeline, args: { name: ci_stage_2.name })

        expect(stage).to eq(ci_stage_2)
      end
    end
  end
end
